# An introduction to linear first-order differential equations.

Differential equations compose an important role not only in mathematics but mechanics, astronomy, and mathematical physics. Solving a differential equation is basically to determine an *unknown function* from prescribed information expressed in the form of an equation involving at least one of the derivatives of the unknown function.

 A simple example of a differential equation is the first-order differential equation for the exponential function $`y'=y`$, which is satisfied by the formula $`y=Ce^{x}`$:

```math
y'=y\\
\frac{y'}{y}=1\\
(\ln y)'=1\\
\ln y + C_1 = x + C_2\\
\ln y = x + C\\
e^{x+C} = y\\
e^xe^C = y\\
Ce^x = y\\
```

The exponential function is equal to its own derivative, and the same is true of any constant multiple of the exponential. To prove that $`y = Ce^x`$ is the only solution, let $`y = g(x)`$ be any solution of this initial-value problem:

```math
g'(x) = g(x) \space \space \textrm{for all x,} \space \space  g(0) = C
```

Consider the function $`h(x) = g(x)e^{-x}`$, we want to show that $`h'(x)`$ is always zero:

```math
h'(x) = g'(x)e^{-x} - g(x)e^{-x} = e^{-x}\left [g'(x) - g(x) \right ] = 0
```

By the [zero-derivative theorem](https://proofwiki.org/wiki/Zero_Derivative_implies_Constant_Function), $`h`$ is constant, and  $`h(0) = g(0)e^0 = C`$, so $`h(x) = C`$ and $`g(x) = Ce^x`$.

A first-order linear differential equation has the following form:

```math
y' + P(x)y = Q(x)
```

where $`P(x)`$ and $`Q(x)`$ are continuous functions on an interval $`I`$.\
\
\
Now, let $`g(x)`$ be our integrating factor and multiply every term by $`g(x)`$:

```math
g(x)y' + g(x)P(x)y = g(x)Q(x)
```

Assuming $`g'(x) = g(x)P(x)`$, we can substitute $`g(x)P(x)`$ in the equation and use the product rule to get $`g(x)y' + g'(x)y = (g(x)y)'`$:

```math
(g(x)y)' = g(x)Q(x)
```

Integrating both sides:

```math
\int{(g(x)y)' \space dx} = \int{Q(x)g(x) \space dx}\\
g(x)y + C_1 = \int{Q(x)g(x) \space dx}\\
y = \frac{\int{Q(x)g(x) \space dx} - C_1}{g(x)}\\
```

Now, we need to find $`g(x)`$ (remember that we determined $`g'(x)`$):

```math
g'(x) = g(x)P(x)\\
\frac{g'(x)}{g(x)} = P(x)\\
(\ln {g(x)})' = P(x)\\
\ln{g(x)} = \int{P(x) \space dx} + C\\
e^{\int{P(x) \space dx} + C} = g(x)\\
e^{\int{P(x) \space dx}}e^C = g(x)\\
e^{\int{P(x) \space dx}}C = g(x)\\
```

Substitute $`g(x)`$:

```math
y = \frac{C_2\int{Q(x)e^{\int{P(x) \space dx}} \space dx} - C_1}{C_2e^{\int{P(x) \space dx} } }\\
y = \frac{\int{Q(x)e^{\int{P(x) \space dx}} \space dx} - \frac{C_1}{C_2}}{e^{\int{P(x) \space dx} } }\\
y = \frac{\int{Q(x)e^{\int{P(x) \space dx}} \space dx} - \frac{C_1}{C_2}}{e^{\int{P(x) \space dx} } }\\
y = \frac{\int{Q(x)e^{\int{P(x) \space dx}} \space dx} - C}{e^{\int{P(x) \space dx} } }\\
```

Then, the solution to a linear first-order differential equation is:

```math
y = \frac{\int{Q(x)g(x) \space dx} - C}{g(x)}
```
where,

```math
g(x) = e^{\int{P(x) \space dx}}
```

You can use the value of $`g(x)`$ to easily solve the differential equation by multiplying all the terms of the equation by $`g(x)`$, and using the product rule to simplify it. Take the following example:

```math
y' - \frac{2}{x}y = x^4
```

We must find the value of $`g(x)`$ first:

```math
g(x) = e^{\int{\frac{2}{x}} \space dx}\\
g(x)= e^{-2\ln x + C}\\
g(x) = Cx^{-2}\\
```

Note: Remember that $`a^x = e^{x \ln a}`$.
\
\
Now solve the equation:

```math
g(x)y' - g(x)\frac{2}{x}y = g(x)x^4\\
Cx^{-2}y' - C\frac{2}{x^3}y = Cx^2\\
(x^{-2}y)' = x^2\\
x^{-2}y + C_1 = \int{x^2 \space dx}\\
x^{-2}y + C_1 = \frac{x^3}{3} + C_2\\
x^{-2}y = \frac{x^3}{3} + C\\
y = \frac{\frac{x^3}{3} + C}{x^{-2}}\\
y = \frac{x^3}{3x^{-2}} + Cx^2\\
y = \frac{x^5}{3} + Cx^2\\
```

This is the general solution to our example, to find the constant $`C`$ we need an initial condition, which are of the form:

```math
y(x_0) = y_0
```

Let's suppose we have an initial value where $`y(1) = 1`$:

```math
1 = \frac{1^5}{3} + C1^2\\
1 = \frac{1}{3} + C\\
\frac{2}{3} = C\\
```

So our actual solution to the initial value problem is:

```math
y = \frac{x^5}{3} + \frac{2x^2}{3}
```

Notes: These examples were taken from Calculus Vol 1 from Apostol and [Paul's Online Notes](https://tutorial.math.lamar.edu/Classes/DE/Linear.aspx), where Apostol has a more detailed explanation about the general solution for first-order linear differential equations, but the result is the same:

```math
Ce^{-A(x)} + e^{-A(x)}\int_{a}^{x} Q(t)e^{A(t)} \space dt \space \space \space \space \space \textrm{where} \space \space \space \space A = \int_{a}^{x}P(t) \space dt\\
\frac{C}{e^{A(x)}} + \frac{\int_{a}^{x} Q(t)e^{A(t)} \space dt}{e^{A(x)}}\\
\frac{\int_{a}^{x} Q(t)e^{A(t)} \space dt + C}{e^{A(x)}}\\
```

## References

1. Tom M. Apostol. CALCULUS. VOLUME 1. One-Variable Calculus, with an. Introduction to Linear Algebra. SECOND EDITION.
2. Paul Dawkins, 2022, Linear Differential Equations, accessed 21 April 2022, <https://tutorial.math.lamar.edu/Classes/DE/Linear.aspx>
